<?php

namespace App\Services;

use App\Modules\Excel\Exports\BaseExport;
use App\Modules\Excel\Imports\BaseImport;
use Exception;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;

class ExcelService {

    /**
     * @param $template
     * @param $data
     *     body - Основное тело отчета.
     *          Вложенные массивы представляют собой строки данных.
     *          Индексы указывают номер строки в Excel.
     *          Внутренние массивы содержат данные по столбцам.
     * @param $reportName
     * @return bool
     * @throws Exception
     */
    public function generate($template, $data, $reportName) {
        return match ($template) {
            'baseTemplate' => Excel::store(new BaseExport($data), '/public/excel/' . $reportName),
            default     => throw new Exception("Неизвестный шаблон: $template"),
        };
    }

    /**
     * Excel файл (либо альтернативный csv) из запроса, в котором есть 3и столбца
     * title (строка)
     * body (строка)
     * created_at (строка ISO 8601/RFC 3339 2020-12-09T16:09:53+00:00)
     * @param $request
     * @return Collection
     */
    public static function sortDataFromExcel($request): Collection {
        // TODO проверить на CSV формате
        $array = Excel::toArray(new BaseImport(), $request->file('excel'))[0];
        $collection = collect();

        foreach ($array as $row) {
            if (isset($row[0], $row[1], $row[2])) {
                try {
                    $date = Carbon::parse($row[2]); // Попытка парсинга даты

                    $collection->push([
                        'title'      => $row[0],
                        'body'       => $row[1],
                        'created_at' => $date,
                    ]);
                } catch (Exception $e) {
                    continue;
                }
            }
        }

        return $collection->sortByDesc('created_at');
    }
}
