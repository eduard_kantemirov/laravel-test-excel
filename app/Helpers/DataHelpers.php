<?php

namespace App\Helpers;

use Exception;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;

class DataHelpers {

    public static function getPublicData(): Collection {
        // TODO надо сделать метод который бы взял данные из публичного апи
        // Url https://techcrunch.com/wp-json/wp/v2/posts?per_page=1
        // Документация https://developer.wordpress.org/rest-api/reference/posts/#list-posts
        $totalPosts = 140;
        $perPage = 70;
        $currentPage = 1;
        $collection = collect();

        while ($collection->count() < $totalPosts) {
            $url = 'https://techcrunch.com/wp-json/wp/v2/posts?per_page=' . $perPage . '&page=' . $currentPage
                . '&orderby=date&_fields=title,content,date';

            try {
                $response = Http::withoutVerifying()->get($url);

                if ($response->successful()) {
                    $data = $response->json();

                    if (empty($data)) {
                        break;
                    }

                    foreach ($data as $row) {
                        $collection->push([
                            'title'      => $row['title']['rendered'],
                            'body'       => $row['content']['rendered'],
                            'created_at' => Carbon::parse($row['date']),
                        ]);

                        if ($collection->count() >= $totalPosts) {
                            break;
                        }
                    }
                } else {
                    return collect(['error' => 'Не удалось получить данные']);
                }

            } catch (Exception $e) {
                return collect(['error' => $e->getMessage()]);
            }
            $currentPage++;
        }
        return $collection->sortByDesc('created_at');
    }
}
