<?php

namespace App\Http\Controllers;

use App\Helpers\DataHelpers;
use App\Services\ExcelService;
use Exception;
use Illuminate\Http\Request;

class ExcelController extends Controller {

    /**
     * @throws Exception
     */
    public function index(Request $request) {
        // на случай если нужно менять название файла
        $fileName = $request->input('fileName', 'Название_файла');

        $extensions = ['xlsx', 'xls', 'csv'];
        if ($request->hasFile('excel') && in_array($request->file('excel')->extension(), $extensions)) {
            // если есть данные, запускаем метод для обработки и сортировки
            $dataRequest = ExcelService::sortDataFromExcel($request);
        } else {
            // если данных нет, запустить метод для запроса данных через
            // Url https://techcrunch.com/wp-json/wp/v2/posts?per_page=1
            // Документация https://developer.wordpress.org/rest-api/reference/posts/#list-posts
            $dataRequest = DataHelpers::getPublicData();
        }

        // в конце создавать excel файл и отдавать его обратно
        $fileName .= '_' . time() . '.xlsx';

        if ((new ExcelService())->generate('baseTemplate', $dataRequest, $fileName)) {
            return asset('/storage/excel/' . $fileName);
        }

        return collect(['error' => 'Не удалось сформировать файл']);
    }
}
