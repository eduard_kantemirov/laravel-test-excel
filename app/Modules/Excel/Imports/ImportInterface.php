<?php

namespace App\Modules\Excel\Imports;


interface ImportInterface {

    public function array(array $rows);

}
