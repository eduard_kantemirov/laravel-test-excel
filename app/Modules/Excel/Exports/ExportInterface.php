<?php

namespace App\Modules\Excel\Exports;

use Illuminate\Support\Collection;

interface ExportInterface {
    public function __construct(Collection $data);
    public function collection(): Collection;
}
