<?php

namespace App\Modules\Excel\Exports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

/**
 * Тут хранится шаблон, нужно чтобы для каждого шаблона был свой класс
 */
class BaseExport implements ExportInterface, FromCollection {

    protected Collection $data;

    public function __construct(Collection $data) {
        $this->data = $data;
    }

    public function collection(): Collection {
        $titles = [];
        $bodies = [];

        foreach ($this->data as $item) {
            $titles[] = $item['title'];
            $bodies[] = $item['body'];
        }

        return new Collection([
            $titles,
            $bodies
        ]);
    }

}
